﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace tpHotel
{
    class Hotel
    {
        private int id;
        private string nom;
        private string adresse;
        private string ville;

        //constructeur
        public Hotel(int unId, string unNom, string uneAdresse, string uneVille)
        {
            this.id = unId;
            this.nom = unNom;
            this.adresse = uneAdresse;
            this.ville = uneVille;
        }
    }
}
